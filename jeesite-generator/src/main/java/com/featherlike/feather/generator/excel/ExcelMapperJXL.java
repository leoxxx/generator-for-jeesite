/*****************************************************************
 *
 *@filename DBExcelMapper.java
 *@author EverWang
 *@date 2013-9-26
 *@copyright (c) 2012-2022  wyyft@163.com All rights reserved.
 *
 *****************************************************************/
package com.featherlike.feather.generator.excel;

import java.io.File;

import jxl.Sheet;
import jxl.Workbook;

/**
 * jxl实现的数据库Excel读取，转换成tableMap
 * @author EverWang
 * @Description
 */
public class ExcelMapperJXL extends ExcelMapper {
   private  Workbook workbook;
   private Sheet sheet;

    public void distroy() {
        workbook.close();
    }

    public String getCellValue(int column, int row) {
        return sheet.getCell(column, row).getContents().trim();
    }

    public void init(String inputPath) throws Exception {
        File file = new File(inputPath);
        workbook = Workbook.getWorkbook(file);
    }

    @Override
    String getSheetName() {
        return sheet.getName();
    }

    @Override
    int getNumberOfSheets() {
        return workbook.getNumberOfSheets();
    }

    @Override
    int getNumOfRows() {
        return sheet.getRows();
    }

    @Override
    void initSheet(int index) {
        sheet = workbook.getSheet(index);

    }
}
